﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ES_1_App
{
    public partial class Form1 : Form
    {
        private DataGridView dataGridView1;
        private Label label1;
        private Button button1;
        private Label label2;
        private Label label3;
        private CheckedListBox checkedListBox1;
        private Label label5;
        private Button button2;
        private Label label4;
        private Button button3; 
        private Button button4;
        private Button button5;
        private DataTable objects;

        private string dbPath;

        public Form1()
        {
            InitializeComponent();

            dbPath = "";
            OpenFileDialog file = new OpenFileDialog();
            if (file.ShowDialog() == DialogResult.OK)
            {
                dbPath = file.FileName;
            }

            DatabaseControl.init(dbPath);

            objects = new DataTable();
            dataGridView1.DataSource = objects;
            objects.Columns.Add("Модель самолета");
            showAllIncludedObjects();

            initFirstQuestion();
        }

        private void initFirstQuestion()
        {
            label2.Text = "Перед вами представлена экспертная система \"Покупка самолета\". В этом окне отображается вопрос, ниже даны ответы. Справа находится список самолетов, которые вам подходят (исходя из ответов).\n[Нажмите кнопку \"Далее\" для продолжения...]";

            checkedListBox1.Items.Clear();
            CheckedListBoxInfo info = new CheckedListBoxInfo { Name = "Здесь отображаются варианты ответов на вопрос... [Выделите для продолжения]", Id = 0, Enabled = true };
            checkedListBox1.Items.Add(info, info.Enabled);
        }

        private void showAllIncludedObjects()
        {
            objects.Clear();

            ArrayList arr = DatabaseControl.getObjectNames();
            for (int i = 0; i < arr.Count; i++)
                objects.LoadDataRow((String[])new ArrayList() { arr[i] }.ToArray(typeof(string)), true);
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }
        
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(756, 25);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView1.ShowCellErrors = false;
            this.dataGridView1.ShowCellToolTips = false;
            this.dataGridView1.ShowEditingIcon = false;
            this.dataGridView1.ShowRowErrors = false;
            this.dataGridView1.Size = new System.Drawing.Size(240, 393);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(756, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Выбранные самолеты:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(921, 424);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Выход";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.Location = new System.Drawing.Point(12, 166);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(738, 130);
            this.label2.TabIndex = 3;
            this.label2.Text = "label2";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Вопрос:";
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(12, 324);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(738, 94);
            this.checkedListBox1.TabIndex = 5;
            this.checkedListBox1.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox1_ItemCheck);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 308);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Ответ:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 424);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "Далее";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.AutoSize = true;
            this.button3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button3.Location = new System.Drawing.Point(93, 424);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(119, 23);
            this.button3.TabIndex = 9;
            this.button3.Text = "Сбросить результат";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.AutoSize = true;
            this.button4.Location = new System.Drawing.Point(766, 424);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(149, 23);
            this.button4.TabIndex = 10;
            this.button4.Text = "Задать порядок вопросов";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.AutoSize = true;
            this.button5.Location = new System.Drawing.Point(535, 424);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(225, 23);
            this.button5.TabIndex = 11;
            this.button5.Text = "Показать подробную таблицу самолетов";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label5.Image = global::ES_1_App.Properties.Resources.es;
            this.label5.Location = new System.Drawing.Point(12, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(738, 131);
            this.label5.TabIndex = 7;
            // 
            // Form1
            // 
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(1008, 453);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.checkedListBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            DatabaseControl.closeDatabase();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            //Проверка, чтобы был выделен один ответ
            bool flag = true;
            for (int ix = 0; ix < checkedListBox1.Items.Count; ++ix)
                if (checkedListBox1.GetItemChecked(ix))
                {
                    flag = false;
                    CheckedListBoxInfo info = checkedListBox1.Items[ix] as CheckedListBoxInfo;
                    if (null != info && info.Id != 0) //если не первый вопрос
                    {
                        DatabaseControl.filterObjects(info.Id);
                    }
                }
            if (flag)
                return;
            
            //Текст вопроса
            DatabaseControl.processQuestion();
            label2.Text = DatabaseControl.getCurrentQuestionText();

            //Текст опций
            ArrayList answerTexts = DatabaseControl.getAnswerTexts(DatabaseControl.currentQuestionID);
            ArrayList answerIDs = DatabaseControl.getAnswerIDs(DatabaseControl.currentQuestionID);
            checkedListBox1.Items.Clear();
            for (int i = 0; i < answerIDs.Count; i++)
            {
                CheckedListBoxInfo info = new CheckedListBoxInfo { Name = (string) answerTexts[i], Id = (int) answerIDs[i], Enabled = false };
                checkedListBox1.Items.Add(info);
                //checkedListBox1.Items.Insert(0, new { Text = answerTexts[i], Id = answerIDs[i].ToString() });
            }
            
            //Показывание объектов
            showAllIncludedObjects();

            //checkedListBox1.getC
        }

        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            for (int ix = 0; ix < checkedListBox1.Items.Count; ++ix)
                if (ix != e.Index) checkedListBox1.SetItemChecked(ix, false);
        }

        public class CheckedListBoxInfo
        {
            public string Name;
            public int Id;
            public bool Enabled;

            public override string ToString()
            {
                return this.Name;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DatabaseControl.closeDatabase();
            DatabaseControl.init(dbPath);

            showAllIncludedObjects();
            initFirstQuestion();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            PlanesTable pt = new PlanesTable();
            pt.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            QuestionPriority pt = new QuestionPriority();
            pt.Show();
        }
    }
}
