﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.Collections;

namespace ES_1_App
{
    class DatabaseControl
    {
        static IDbConnection dbconn;
        static IDbCommand dbcmd;
        static IDataReader reader;

        static ArrayList objectIDs;

        static public string currentQuestionText;
        static public int currentQuestionID;
        static public int nextQuestionID;

        static public void init(string path)
        {
            currentQuestionID = 0;
            nextQuestionID = 0;

            initDatabase(path);
            resetDatabase();

            initObjects();
        }

        static public void processQuestion()
        {
            currentQuestionID = nextQuestionID;
            askQuestion();
        }

        static public void askQuestion() //return next question
        {
            //int nextQuestionNumber = 0;
            //int currentQuestionId = 0;

            if (currentQuestionID == 0) //если первый вопрос
            {
                reader = sqlQuery("SELECT id, text, asked, priority, id_next_question FROM Question WHERE asked = 0 and start = " + 1);

                if (reader.Read()) //если по заданной последовательности
                {
                    int id = reader.GetInt32(0);
                    string text = reader.GetString(1);
                    int asked = reader.GetInt32(2);
                    int p = reader.GetInt32(3);

                    if (!reader.IsDBNull(4))
                        nextQuestionID = reader.GetInt32(4);

                    reader = sqlQuery("UPDATE Question SET asked = 1 WHERE id = " + id);
                    currentQuestionID = id;
                    currentQuestionText = text;
                    Console.WriteLine(text);
                }
            }
            else
            {
                reader = sqlQuery("SELECT id, text, asked, priority, id_next_question FROM Question WHERE asked = 0 and id = " + currentQuestionID);

                if (reader.Read()) //если по заданной последовательности
                {
                    int id = reader.GetInt32(0);
                    string text = reader.GetString(1);
                    int asked = reader.GetInt32(2);
                    int p = reader.GetInt32(3);

                    if (!reader.IsDBNull(4))
                        nextQuestionID = reader.GetInt32(4);

                    reader = sqlQuery("UPDATE Question SET asked = 1 WHERE id = " + id);
                    currentQuestionID = id;
                    currentQuestionText = text;

                    Console.WriteLine(text);
                }
                else //если по приоритету
                {
                    reader = sqlQuery("SELECT id, text, asked, priority, id_next_question FROM Question WHERE asked = 0 ORDER BY priority");

                    if (reader.Read())
                    {
                        int id = reader.GetInt32(0);
                        string text = reader.GetString(1);
                        int asked = reader.GetInt32(2);
                        int p = reader.GetInt32(3);

                        if (!reader.IsDBNull(4))
                            nextQuestionID = reader.GetInt32(4);
                        currentQuestionID = id;
                        currentQuestionText = text;

                        reader = sqlQuery("UPDATE Question SET asked = 1 WHERE id = " + id);

                        Console.WriteLine(id + " " + text);
                    }
                }
            }
        }

        static public ArrayList getAnswerIDs(int questionId)
        {
            reader = sqlQuery(@"SELECT DISTINCT o.id_option_name, o.id, op.name FROM Question as q, Option as o, 
                                OptionName as op, Object as obj, ObjectsPerOption opo
                                where q.id = o.id_question and o.id_option_name = op.id and obj.id = opo.id_object and opo.id_option_name = op.id and obj.included = 1 and q.id = " + questionId);

            ArrayList answerIDs = new ArrayList();
            while (reader.Read())
            {
                int id_option_name = reader.GetInt32(0);
                int id = reader.GetInt32(1);
                string text = reader.GetString(2);

                answerIDs.Add(id_option_name);
            }

            return answerIDs;
        }

        static public ArrayList getAnswerTexts(int questionId)
        {
            reader = sqlQuery(@"SELECT DISTINCT o.id_option_name, o.id, op.name FROM Question as q, Option as o, 
                                OptionName as op, Object as obj, ObjectsPerOption opo
                                where q.id = o.id_question and o.id_option_name = op.id and obj.id = opo.id_object and opo.id_option_name = op.id and obj.included = 1 and q.id = " + questionId);

            ArrayList answerTexts = new ArrayList();
            while (reader.Read())
            {
                int id_option_name = reader.GetInt32(0);
                int id = reader.GetInt32(1);
                string text = reader.GetString(2);

                answerTexts.Add(text);
            }

            return answerTexts;
        }

        static public ArrayList getObjectNames()
        {
            ArrayList arr = new ArrayList();
            IDataReader reader = sqlQuery("SELECT name FROM Object WHERE included = 1 ORDER BY id");
            while (reader.Read())
            {
                string name = reader.GetString(0);
                arr.Add(name);
            }

            return arr;
        }

        static public string getCurrentQuestionText()
        {
            return currentQuestionText;
        }

        static public void filterObjects(int answerID)
        {
            if (answerID == 0)
                return;

            reader = sqlQuery("SELECT obj.id, obj.name FROM Object as obj, ObjectsPerOption as opo where obj.id = opo.id_object and obj.included = 1 and opo.id_option_name = " + answerID);

            ArrayList includedIDs = new ArrayList();

            int count = 0;
            while (reader.Read())
            {
                int id = reader.GetInt32(0);
                string name = reader.GetString(1);

                includedIDs.Add(id);
                count++;
                //answerIDs.Add(id);
            }

            if (includedIDs.Count != 0)
            {
                string queryStr = "UPDATE Object SET included = 0 WHERE";
                for (int i = 0; i < includedIDs.Count - 1; i++)
                    queryStr += " not id = " + includedIDs[i] + " and ";
                queryStr += " not id = " + includedIDs[includedIDs.Count - 1];

                reader = sqlQuery(queryStr);
            }
        }

        static public List<List<string>> getAllObjectsInfo()
        {
            List<List<string>> arr = new List<List<string>>();

            reader = sqlQuery("SELECT obj.name, opn.category, opn.name from Object as obj, OptionName as opn, ObjectsPerOption opo WHERE opo.id_object = obj.id AND opo.id_option_name = opn.id ORDER BY obj.name");
            while (reader.Read())
            {
                string name = reader.GetString(0);
                string category = reader.GetString(1);
                string name1 = reader.GetString(2);

                List<string> arr1 = new List<string>();
                arr1.Add(name);
                arr1.Add(category);
                arr1.Add(name1);

                arr.Add(arr1);
            }
            
            return arr;
        }

        static public List<List<string>> getAllQuestionsInfo()
        {
            List<List<string>> arr = new List<List<string>>();

            reader = sqlQuery("SELECT id, text, priority, id_next_question, start from Question ORDER BY id");
            while (reader.Read())
            {
                int id = reader.GetInt32(0);
                string text = reader.GetString(1);
                int priority = reader.GetInt32(2);
                
                
                List<string> arr1 = new List<string>();
                arr1.Add(id.ToString());
                arr1.Add(text);
                arr1.Add(priority.ToString());

                if (reader.IsDBNull(3)) //id_next_question
                    arr1.Add("-");
                else
                    arr1.Add(reader.GetInt32(3).ToString());

                if (reader.IsDBNull(4)) //start
                    arr1.Add("-");
                else
                    arr1.Add(reader.GetInt32(4).ToString());
                
                arr.Add(arr1);
            }

            return arr;
        }

        static public void resetDatabase()
        {
            reader = sqlQuery("UPDATE Question SET asked = 0");
            reader = sqlQuery("UPDATE Object SET included = 1");
        }

        static public void initObjects()
        {
            objectIDs = new ArrayList();

            IDataReader reader = sqlQuery("SELECT id FROM Object ORDER BY id");

            while (reader.Read())
            {
                int id = reader.GetInt32(0);
                objectIDs.Add(id);
            }
        }

        static public IDataReader sqlQuery(string query)
        {
            if (reader != null)
                reader.Close();

            dbcmd.Dispose();
            dbcmd = null;
            dbcmd = dbconn.CreateCommand();

            dbcmd.CommandText = query;
            reader = dbcmd.ExecuteReader();

            return reader;
        }

        static public void closeDatabase()
        {
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            dbconn.Close();
            dbconn = null;
        }

        static public void initDatabase(string path)
        {
            //string conn = "URI=file:" + System.IO.Directory.GetCurrentDirectory() + "/es.db"; //Path to database.
            string conn = "URI=file:" + path; //Path to database.
            dbconn = (IDbConnection)new SQLiteConnection(conn);
            dbconn.Open(); //Open connection to the database.
            dbcmd = dbconn.CreateCommand();
        }

    }


}
